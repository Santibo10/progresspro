from django import forms

# login form
class LoginForm(forms.Form):
    username = forms.CharField(max_length=50)
    password = forms.CharField(
        max_length=50,
        widget = forms.PasswordInput
    )

# signup form
class SignupForm(forms.Form):
    email = forms.EmailField(
        max_length=200,
        widget=forms.EmailInput,
    )
    username = forms.CharField(max_length=50)
    password = forms.CharField(
        max_length=50,
        widget = forms.PasswordInput
    )
    password_confirm = forms.CharField(
        max_length=50,
        widget = forms.PasswordInput
    )
