from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from .forms import LoginForm, SignupForm
from datetime import datetime
from django.contrib.auth.models import User


def the_date():
    return datetime.today()

def log_in(request):

    # redirect is already logged in
    if request.user.is_authenticated:
        return redirect("home", the_date().year, the_date().month)

    if request.method == "POST":
        form = LoginForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("home", the_date().year, the_date().month)
            else:
                form.add_error("username", "The Username or Password is incorrect.")
    else:
        form = LoginForm()

    context = {
        "form":form,
    }
    return render(request, "accounts/login.html", context)

def log_out(request):
    logout(request)
    return redirect("login")


def sign_up(request):
    # redirect is already logged in
    if request.user.is_authenticated:
        return redirect("home", the_date().year, the_date().month)

    if request.method == "POST":
        form = SignupForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirm = form.cleaned_data["password_confirm"]
            email = form.cleaned_data["email"]
            if User.objects.filter(email=email).exists():
                form.add_error("email", "Email already registered.")
            else:
                if password == password_confirm:

                    user = User.objects.create_user(username=username, password=password, email=email)
                    login(request, user)
                    return redirect("home", the_date().year, the_date().month)

                else:
                    form.add_error("password", "Password fields do not match.")
    else:
        form = SignupForm()

    context = {
        "form":form,
    }
    return render(request, "accounts/signup.html", context)
