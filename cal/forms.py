from .models import Event, Tag
from django import forms
from .utils import the_date



class EventForm(forms.ModelForm):
    date = forms.DateTimeField(
        widget=forms.SelectDateWidget,
        initial = the_date()
    )
    class Meta:
        model = Event
        fields = [
            "title",
            "description",
            "date",
            "tag",
            "is_complete",
        ]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user", None)
        super().__init__(*args, **kwargs)
        if user:
            self.fields["tag"].queryset = Tag.objects.filter(owner=user)


class TagForm(forms.ModelForm):
    class Meta:
        model = Tag
        fields = [
            "name",
            "color",
        ]
