from django.core.management.base import BaseCommand
from cal.models import Event
from datetime import datetime
import smtplib


# --------EMAIL DATA------#
BOT_EMAIL = "my.progress.pro@gmail.com"
PASSWORD = "omortbpzkduwxfwa"
SMTP_SETTING = "smtp.gmail.com"

# ---------SEND EMAIL---------------#
def send_mail(email_address, message, event_name):

    with smtplib.SMTP(SMTP_SETTING, port=587) as medium:
        medium.starttls()
        medium.login(user=BOT_EMAIL, password=PASSWORD)

        medium.sendmail(
            from_addr=BOT_EMAIL,
            to_addrs=email_address,
            msg=f"Subject:{event_name} today!\n\n{message}",
        )

# -------DUE MESSAGE----------#
def due_message(event_name, owner):
    intro = f"Hey, {owner}\n\n"
    body = f"Your scheduled event, {event_name}, is due today!\n"
    outro = "Make sure you make enough time to get it done!\n"
    return intro + body + outro

class Command(BaseCommand):
    def handle(self, *args, **options):
        emails_sent = 0
        events = Event.objects.filter(is_complete=False)
        today = datetime.today().strftime("%Y-%m-%d")
        for event in events:
            if event.date.strftime("%Y-%m-%d") == today:
                event.is_complete = True
                event.save()
                message = due_message(event.title, event.owner)
                send_mail(event.owner.email, message, event.title)
                emails_sent += 1
                print(emails_sent)
