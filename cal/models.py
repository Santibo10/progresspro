from django.db import models
from django.conf import settings
from .utils import tag_colors



class Tag(models.Model):
    name = models.CharField(max_length=100)
    color_choices = tag_colors
    color = models.CharField(max_length=100, choices=color_choices)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tags",
        on_delete=models.CASCADE,
        null = True,
    )

    def __str__(self):
        return self.name

class Event(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(blank=True, default="Optional Description Here")
    date = models.DateTimeField()
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="events",
        on_delete=models.CASCADE,
        null = True
    )
    tag = models.ForeignKey(
        Tag,
        related_name="tag",
        on_delete=models.CASCADE,
        null = True,
        blank=True
    )
    is_complete = models.BooleanField(default=False)
