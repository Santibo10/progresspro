from django.urls import path
from cal.views.cal_views import my_calendar, day_details
from cal.views.tag_views import tags
from cal.views.event_views import event_details

urlpatterns = [
    path("<int:year>/<str:month>/", my_calendar, name="home"),
    path("<int:year>/<str:month>/<int:day>/", day_details, name="day_details"),
    path("<str:action>/<int:id>/", event_details, name="event_details"),
    path("tags/<str:action>/<int:id>/", tags, name="tags"),
]
