from datetime import datetime
import pytz

# -------COLORS--------#
tag_colors = [
        ("#71568C","Royalty"),
        ("#68A6A0","Evergreen"),
        ("#184C78","Seabreeze"),
        ("#AB5D1B","Sunset"),
        ("#983222","Danger"),
        ("#B88B00","Meloncoly"),
        ("#1E1C1A","Shadows"),
        ("#737166","Skyrise")]


# Functions
def calculate_month(month, number):
    if month + number > 12:
        month = 1
    elif month + number < 1:
        month = 12
    else:
        month += number
    return month

def the_date():
    now = datetime.now()
    denver_time = pytz.timezone("America/Denver")
    return now.astimezone(denver_time)

def convert_date():
    date = datetime.now()
    universal_time = pytz.timezone("UTC")
    return date.astimezone(universal_time)
