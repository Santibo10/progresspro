from django.shortcuts import render, redirect
import calendar as Calendar
from cal.models import Event
from cal.forms import EventForm
from cal.utils import calculate_month, the_date
from django.contrib.auth.decorators import login_required
# update_logs

# Calendar View
@login_required
def my_calendar(request, year, month):
    print(the_date())
    if request.method == "POST":
        form = EventForm(request.POST, user=request.user)
        if form.is_valid():
            item = form.save(False)
            item.owner = request.user
            item.save()
            return redirect("home", the_date().year, the_date().month)
    else:
        form = EventForm(user=request.user)

    year, month = int(year), int(month)
    cal_dates =  Calendar.HTMLCalendar().itermonthdates(year, month)
    events = Event.objects.filter(date__month=month, date__year=year, owner=request.user)
    context = {
        "today": the_date().date,
        "calendar": cal_dates,
        "events":events,
        "last_year": year - 1,
        "last_month": calculate_month(month, -1),
        "next_year": year + 1,
        "next_month": calculate_month(month, 1),
        "year": year,
        "month":month,
        "month_name": Calendar.month_name[month],
        "tag_acction":"create",
        "form":form,
    }
    return render(request, 'cal/calendar.html', context)



# detailed views
@login_required
def day_details(request, year, month, day):
    year, month = int(year), int(month)
    events = Event.objects.filter(date__month=month, date__year=year, date__day=day, owner=request.user)
    context = {
        "events":events,
        "year":year,
        "month":month,
        "day":day,
    }
    return render(request, "cal/day_detail.html", context)
