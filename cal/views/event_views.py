from django.shortcuts import render, get_object_or_404, redirect
from cal.models import Event
from cal.forms import EventForm
from cal.utils import the_date, convert_date
from django.contrib.auth.decorators import login_required
from django.http import Http404

# events Update/read/delete
@login_required
def event_details(request, action, id):
    try:
        event = get_object_or_404(Event, id=id)
    except Http404:
        return redirect("home", the_date().year, the_date().month)

    if request.method == "POST":
        if action == "delete":
            event.delete()
            return redirect("home", the_date().year, the_date().month)

        else:
            form = EventForm(request.POST, instance=event)
            if form.is_valid():
                form.save()

                return redirect("home", the_date().year, the_date().month)

    else:
        form = EventForm(instance=event, user=request.user)

    days_left = (convert_date() - event.date).days * -1
    context = {
        "event":event,
        "action":action,
        "form":form,
        "year":the_date().year,
        "month":the_date().month,
        "days_left":days_left,
    }
    return render(request, "cal/event_detail.html", context)
