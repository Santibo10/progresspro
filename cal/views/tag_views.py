from django.shortcuts import render, get_object_or_404, redirect
from cal.models import Event, Tag
from cal.forms import TagForm
from cal.utils import the_date
from django.contrib.auth.decorators import login_required
from django.http import Http404

def constants_for_tag_view(user):
    my_tags = Tag.objects.filter(owner=user)
    events = Event.objects.filter(is_complete=False, owner=user)
    year = the_date().year
    month = the_date().month
    return my_tags, events, year, month

# CRUD for Tags
@login_required
def tags(request, action, id):
    tag = None
    if action == "edit" or action == "delete":
        try:
            tag = get_object_or_404(Tag, id=id)
        except Http404:
            print("wrong")
            return redirect("tags", "create", 1 )

    if request.method == "POST":
        if action == "delete":
            tag.delete()
            return redirect("tags", "create", 1 )
        if action == "edit":
            form = TagForm(request.POST, instance=tag)
        else:
            form = TagForm(request.POST)
        if form.is_valid():
            item = form.save(False)
            item.owner = request.user
            item.save()
            return redirect("tags", "create", 1 )
    else:
        my_tags, events, year, month = constants_for_tag_view(request.user)
        if action == "edit":
            form = TagForm(instance=tag)
        else:
            form = TagForm()
    context = {
        "form":form,
        "tags":my_tags,
        "events":events,
        "action":action.title(),
        "year":year,
        "month":month,
        "tag":tag,
    }
    return render(request, "cal/tags.html", context)
